from points_io import save_points_as_pdb

points = [(0.000, 0.000, 0.000),
          (0.035, 0.831, -0.576),
          (-0.907, 1.388, -0.795),
          (0.017, 1.666, -1.599),
          (-0.106, 1.856, -2.409)]

save_points_as_pdb(points, "example.pdb")